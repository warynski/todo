import { NgToDoPage } from './app.po';

describe('ng-to-do App', () => {
  let page: NgToDoPage;

  beforeEach(() => {
    page = new NgToDoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
